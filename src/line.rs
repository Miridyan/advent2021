use crate::Point;
use std::{default::Default, iter::Iterator};

#[derive(PartialEq)]
pub enum Orientation {
    Vertical,
    Horizontal,
    Diagonal,
}

#[derive(Debug)]
pub struct Line {
    pub start: Point<i32>,
    pub end: Point<i32>,
}

pub struct StraightLineIterator<'a> {
    line: &'a Line,
    current: Point<i32>,
    movement: Point<i32>,
    can_continue: bool,
}

impl<'a> Line {
    pub fn orientation(&self) -> Orientation {
        let (dx, dy) = (self.end.0 - self.start.0, self.end.1 - self.start.1);

        match (dx, dy) {
            (0, _) => Orientation::Vertical,
            (_, 0) => Orientation::Horizontal,
            _ => Orientation::Diagonal,
        }
    }

    pub fn sl_iter(&'a self) -> Option<StraightLineIterator<'a>> {
        let (start_x, start_y) = self.start;
        let (end_x, end_y) = self.end;
        let (dx, dy) = (end_x - start_x, end_y - start_y);

        if dx == 0 && dy == 0 {
            return None;
        }

        let (mx, my) = (
            dx.abs().checked_div(dx).unwrap_or(0),
            dy.abs().checked_div(dy).unwrap_or(0),
        );

        Some(StraightLineIterator {
            line: &self,
            current: self.start,
            movement: (mx, my),
            can_continue: true,
        })
    }
}

impl Default for Line {
    fn default() -> Self {
        Self {
            start: (i32::MAX, i32::MAX),
            end: (i32::MIN, i32::MIN),
        }
    }
}

impl<'a> Iterator for StraightLineIterator<'a> {
    type Item = Point<i32>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.can_continue {
            if self.line.end == self.current {
                self.can_continue = false;
            }

            let (current_x, current_y) = self.current;
            let (move_x, move_y) = self.movement;
            self.current = (current_x + move_x, current_y + move_y);

            return Some((current_x, current_y));
        }
        None
    }
}
