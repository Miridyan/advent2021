pub mod error;
pub mod line;

pub type Pair<T> = (T, T);
pub type Point<T> = Pair<T>;
pub type Result<T> = std::result::Result<T, error::Error>;

#[derive(Debug)]
pub enum Direction {
    Up(i32),
    Down(i32),
    Forward(i32),
    Backward(i32),
}

impl Direction {
    pub fn from_str(input: &str) -> Option<Self> {
        let mut parts = input.split(" ");

        let first = parts.next()?;
        let second = parts.next()?;
        let magnitude = i32::from_str_radix(second, 10).ok()?;

        match first {
            "backward" => Some(Self::Backward(magnitude)),
            "forward" => Some(Self::Forward(magnitude)),
            "down" => Some(Self::Down(magnitude)),
            "up" => Some(Self::Up(magnitude)),
            _ => None,
        }
    }
}
