use advent::Result;
use std::{
    fs::File,
    io::{prelude::*, BufReader},
    str::from_utf8,
};

fn calculate_generation(init: &Vec<i32>, num_generations: usize) -> Vec<u64> {
    let mut generation_counts = vec![0u64; 9];
    for &item in init.iter() {
        generation_counts[item as usize] += 1;
    }

    for _ in 1..=num_generations {
        let zeroes = generation_counts.remove(0);
        generation_counts.push(0);

        generation_counts[6] += zeroes;
        generation_counts[8] += zeroes;
    }

    generation_counts
}

fn main() -> Result<()> {
    let file = File::open("input/day6")?;
    let input = BufReader::new(&file)
        .split(b',')
        .filter_map(|item| {
            let u8_vec = item.ok()?;
            let utf8_str = from_utf8(u8_vec.as_slice()).ok()?;
            let line = utf8_str.lines().next()?; // account for newline char at EOF

            i32::from_str_radix(line, 10).ok()
        })
        .collect::<Vec<i32>>();

    let part1 = calculate_generation(&input, 80)
        .iter()
        .fold(0, |acc, x| acc + x);
    let part2 = calculate_generation(&input, 256)
        .iter()
        .fold(0, |acc, x| acc + x);

    println!("Part 1 ::: {}", part1);
    println!("Part 2 ::: {}", part2);

    Ok(())
}
