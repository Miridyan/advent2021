use advent::{Result, line::Line};
use std::{
    cmp,
    default::Default,
    fs::File,
    io::{prelude::*, BufReader},
};

fn main() -> Result<()> {
    let file = File::open("input/day5")?;
    let lines = BufReader::new(file)
        .lines()
        .filter_map(move |line| {
            let line = line.ok()?;

            let mut values = line.split(" -> ");
            let mut first = values.next()?.split(",");
            let mut second = values.next()?.split(",");

            let (x1, y1) = (first.next()?, first.next()?);
            let (x2, y2) = (second.next()?, second.next()?);

            let start = (
                i32::from_str_radix(x1, 10).ok()?,
                i32::from_str_radix(y1, 10).ok()?,
            );
            let end = (
                i32::from_str_radix(x2, 10).ok()?,
                i32::from_str_radix(y2, 10).ok()?,
            );

            Some(Line { start, end })
        })
        .collect::<Vec<Line>>();

    let Line {
        start: (min_x, min_y),
        end: (max_x, max_y),
    } = lines.iter().fold(Line::default(), |acc, line| {
        let Line {
            start: (x1, y1),
            end: (x2, y2),
        } = line;

        Line {
            start: (
                cmp::min(acc.start.0, cmp::min(*x1, *x2)),
                cmp::min(acc.start.1, cmp::min(*y1, *y2)),
            ),
            end: (
                cmp::max(acc.end.0, cmp::max(*x1, *x2)),
                cmp::max(acc.end.1, cmp::max(*y1, *y2)),
            ),
        }
    });

    let (width, height) = (max_x - min_x, max_y - min_y);
    let mut map1 = vec![vec![0; (height + 1) as usize]; (width + 1) as usize];
    let mut map2 = vec![vec![0; (height + 1) as usize]; (width + 1) as usize];

    for line in lines.iter() {
        for (x, y) in line.sl_iter().unwrap() {
            use advent::line::Orientation::*;

            match line.orientation() {
                Horizontal | Vertical => {
                    map1[(x - min_x) as usize][(y - min_y) as usize] += 1
                }
                Diagonal => {
                    map2[(x - min_x) as usize][(y - min_y) as usize] += 1
                }
            }
        }
    }

    let (part1, part2) = map1.iter().flatten().zip(map2.iter().flatten()).fold(
        (0, 0),
        |(mut part1, mut part2), (&first, &second)| {
            if first >= 2 {
                part1 += 1;
            }

            if first + second >= 2 {
                part2 += 1;
            }

            (part1, part2)
        },
    );

    println!("Part 1 ::: {}", part1);
    println!("Part 2 ::: {}", part2);

    Ok(())
}
